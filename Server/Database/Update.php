<?php
abstract class Update{
	//Individual values *powers of 2
	const NOTHING = 0;
	const COUNT = 1;
	const START_TIME = 2;
	const END_TIME = 4;
	const MIN_SIGNAL = 8;
	const MAX_SIGNAL = 16;
	//Combination values
	const COUNT_START = 3;
	const COUNT_END = 5;
	const COUNT_MIN = 8;
	const COUNT_MAX = 17;
	const COUNT_START_MIN = 11;
	const COUNT_START_MAX = 19;
	const COUNT_END_MIN = 13;
	const COUNT_END_MAX = 21; 
}
?>