<?php
ini_set('memory_limit', '-1');
set_time_limit(0);
ini_set('mysql.connect_timeout', 300);
ini_set('default_socket_timeout', 300);
date_default_timezone_set('America/New_York');

//Helper functions to connect to the database, database.ini contains info

    //Function to connect to db server without specifying current working db
    function connect_sans_db()
    {
        static $connection;
        if(!isset($connection))
        {
            $config = parse_ini_file('../Credentials/credentials.ini'); 
            $connection = mysqli_connect($config['server'], $config['username'], $config['password']);
        }
        if($connection === false)
        {
            return mysqli_connect_error(); 
        }
        return $connection;
    }

    //Function to connect to db server with working db specified
    function connect_w_db()
    {
        static $conn;
        $conn = connect_sans_db();
        $config = parse_ini_file('../Credentials/credentials.ini');
        mysqli_select_db($conn, $config['database']);
        return $conn;
    }

    function mysqli_result($res, $row = 0 , $col = 0)
    { 
    	$numrows = mysqli_num_rows($res); 
    	if ($numrows && $row <= ($numrows-1) && $row >=0)
    	{
	        mysqli_data_seek($res,$row);
	        $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
	        if (isset($resrow[$col]))
            {   return $resrow[$col];}
        }
        return false;
    }
?>