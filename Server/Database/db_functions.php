<?php
header('Content-Type: text/html; charset=utf-8');
#db_functions.php
#Helper functions that may or may not save on writing duplicate code
require '../Device/device.php';

//takes data and group into a Device object, returns object
function create_Device($Pi_Mac, $Device_Hash, $Manfact_Prefix, $Visit_Time, $Visit_DB, $Message_Id, $Receipt_Handle)
{
	return new device($Pi_Mac, $Device_Hash, $Manfact_Prefix, $Visit_Time, $Visit_DB, $Message_Id, $Receipt_Handle);
}

//sql to insert many devices into VISIT table (**Values clause can only hold 1000 different entries**)
function insert_many_in_Visit($db_connection, $Devices)
{
	$Receipt_arry = array(); //array to hold IDs and Reciept Handles
	//first part of sql
	$sql = "INSERT IGNORE INTO VISIT(`DEVICE_HASH`, `MANFACT_PREFIX`, `LOC_ID`, `VISIT_TIME`, `VISIT_DB`) VALUES";

	foreach ($Devices as $Device){
		$sql .= " ('" . $Device->gDevice_Hash() . "','" . $Device->gManfact_Prefix() . "'," . find_Pi_Loc($db_connection, $Device->gPi_Mac()) . ", '" . parseMyDate($Device->gVisit_Time()) . "','" . $Device->gVisit_DB() . "'),";
		$Receipt_arry[] = create_receipt($Device->gMessage_Id(), $Device->gReceiptHandle());
	}
	unset($Device);
	//need to substring to take off the last ',' otherwise sql statement would be incorrect
	$sql = substr($sql, 0, -1) . ";";
	//returns true or false based on success of query
	$batch_okay = mysqli_query($db_connection, $sql);
	if(!$batch_okay){
		//If the batch query fails then each device will be written to the db one at a time, 
		$Receipt_arry = array();
		foreach ($Devices as $Device){
			$okay = insert_1_in_Visit($db_connection, $Device);
			//return message id and reciept handle based on what is returned, returned either true or false
			if($okay){
				$Receipt_arry[] = create_receipt($Device->gMessage_Id(), $Device->gReceiptHandle());
			}
		}
		return $Receipt_arry;
	}
	else{
		return $Receipt_arry;
	}
}

//sql to insert 1 device into VISIT table, Used as a safety function if the batch insert fails for any reason
function insert_1_in_Visit($db_connection, $Device)
{
	$Pi_Loc = find_Pi_Loc($db_connection, $Device->gPi_Mac());
	$sql = "INSERT IGNORE INTO VISIT(`DEVICE_HASH`, `MANFACT_PREFIX`, `LOC_ID`, `VISIT_TIME`, `VISIT_DB`) VALUES ('" . $Device->gDevice_Hash() . "','" . $Device->gManfact_Prefix() . "'," . $Pi_Loc . ",'" . parseMyDate($Device->gVisit_Time()) . "','" . $Device->gVisit_DB() . "');";
	return mysqli_query($db_connection, $sql); // returns true or false based on whether or not the query was successful
}

//returns Loc_Id associated with Pi_Mac 
function find_Pi_Loc($db_connection, $Pi_Mac)
{
	$sql = "SELECT `LOC_ID` FROM PI_LOC_MAP WHERE PI_MAC ='" . $Pi_Mac . "';";
	$number = mysqli_query($db_connection, $sql)->fetch_object()->LOC_ID;
	if($number >= 1){
		return $number;
	}
	else{
		return "NULL";
	}
}

function find_LOC_name($db_connection, $loc_id){
	$sql_stmt = $db_connection->prepare("SELECT LOC_NAME FROM LOCATION WHERE LOC_ID = ?");
	$sql_stmt->bind_param("i", $loc_id);
	$sql_stmt->execute();
	$sql_stmt->bind_result($loc_name);
	$sql_stmt->fetch();
	$sql_stmt->close();
	return $loc_name;
}

//this function sounds out of place, so let me explain: to properly delete a message, one needs an object that contains a message_id and reciept handle, this creates said object the way AWS expects to see it allowing us to delete the message after a proper insertion.
function create_receipt($message_id, $receipt_handle)
{
	$object = new stdClass();
	$object->message_id = $message_id;
	$object->receipt_handle = $receipt_handle;
	return $object;
}

//This updates the sign-in table with an accurate timestamp, but because the sqs queue that we pull from for the information is not in order, we need to check to make sure the timestamp is later (greater) than the current timestamp for that pi, location combo. SOMETHING TO NOTE: If a pi is updated with a new location then the pair will be different to the old one in this table. Make sure to delete the old record otherwise you might get emails with outdated information.
function sign_in($db_connection, $pi_mac, $timestamp)
{
	/*COPY THIS FORMAT
	INSERT INTO SIGN_IN(`PI_MAC`, `LOC_ID`, `SIGN_IN_TIME`) 
	values ('b8:27:eb:7f:ac:07', 3, '2016-10-14 10:45:03') 
    ON duplicate key update SIGN_IN_TIME = CASE 
		WHEN SIGN_IN_TIME < '2016-10-14 10:44:03'THEN '2016-10-14 10:44:03' 
		ELSE SIGN_IN_TIME END;
	*/
	$pi_loc = find_Pi_Loc($db_connection, $pi_mac);
	$sql = "INSERT INTO SIGN_IN (`PI_MAC`, `LOC_ID`, `SIGN_IN_TIME`) values ('".$pi_mac."',".$pi_loc.",'".$timestamp."') ON DUPLICATE KEY UPDATE SIGN_IN_TIME = CASE WHEN SIGN_IN_TIME < '".$timestamp."' THEN '".$timestamp."' ELSE SIGN_IN_TIME END;" ;
	$result = mysqli_query($db_connection, $sql);
	return $result;
}

//returns the properly formatted date for SQL database kinda hacky
function parseMyDate($visit_time)
{
	list($month_date, $year_time) = explode(",", $visit_time);
	$year_time = substr($year_time,0,-4);
	$parts = preg_split('/\s+/', $month_date); // get rid of the extra spaces
	$month = $parts[0];
	$day = $parts[1];

	switch($month){
		case "Jan":
			$month = "01";
			break;
		case "Feb":
			$month = "02";
			break;
		case "Mar":
			$month = "03";
			break;
		case "Apr":
			$month = "04";
			break;
		case "May":
			$month = "05";
			break;
		case "Jun":
			$month = "06";
			break;
		case "Jul":
			$month = "07";
			break;
		case "Aug":
			$month = "08";
			break;
		case "Sep":
			$month = "09";
			break;
		case "Oct":
			$month = "10";
			break;
		case "Nov":
			$month = "11";
			break;
		case "Dec":
			$month = "12";
			break;
	}
	list($blank, $year, $splitTime) = explode(' ', $year_time); //seperate year from time
	list($time, $microTime) = explode('.', $splitTime); //truncate the decimal point (no microseconds)
	$visit_time = $year . "-" . $month . "-" . $day . " " . $time; //concatinate the new date string
	return $visit_time; //return the proper date format
}
?>