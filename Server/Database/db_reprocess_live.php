<?php
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('America/New_York');
require 'Update.php';
require 'db_functions.php';
require 'connect.php';

#db_reprocess.php
#Each instance of a device sent through the SQS is a snapshot in time, this file will take all the instances that have been collected and insert them into a table that better represents a device over time. This file will be incorperated with the main database function db_write
define('CONVERSION', 60);
define('SECS_N_DAY', 86400); /*Number of seconds in a day*/
define('VISIT_THRESHOLD', 4 * CONVERSION * CONVERSION); /*4 hours in seconds*/

function process($device)
{
    /*Main function: This function takes in a $device object that has all the neccessary info to evaluate a current visit. Visits are the expanded version of a device instance.*/
    global $db_connection;
    //global $log_file;
    global $handler_arr;

    //Visit_Time is a DateTime object and needs to be split into Date and Time
    list($date, $time) = explode(" ", $device->gVisit_Time());
    $device->sDate($date);
    $device->sTime($time);

    //$loc_id = find_Pi_Loc($db_connection, $device->gPi_Mac());
    $loc_id = intval($device->gPi_Mac());
 
    /*Default action is to insert the device instance as a visit, this is the prepared sql statement's params*/
    $default_insert_params = array("ssisssiss", $device->gDevice_Hash(), $device->gManfact_Prefix(), $loc_id, $device->gDate(), $device->gTime(), $device->gTime(), $count = 1, $device->gVisit_DB(), $device->gVisit_DB());

    /*Select all rows from BESUCH that have the same device_hash & visit_date as the device instance being passed in*/
    $result = bind_exec($db_connection, 'Select', $handler_arr['visits'], array("ss", $device->gDevice_Hash(), $device->gDate()));

    #fwrite($log_file, microtime() . ": The number of results returned from BESUCH: " . $result->num_rows . "\n");
    /*
        Based on the number of rows returned from the select stmt above we will do 1 of 3 things:
            If 0 returned: Insert device instance into BESUCH table with prepared insert stmt from line 46
            If 1 returned: Evaluate the returned visit and determine what values need to updated (if any), or if there needs to be a split. 
            If 2 returned: Evaluate all returned and find the closest & most relevant visit to alter in relation to the device instance that is being passed.
            *A split is a situation where the device instance being passed in is in between the start_time and the end_time of a visit that was returned, split is the process that takes the visit and seperates into two different visits with the device instance as the divider.
    */
    //Logic on what to do based on the number of rows returned, cases listed above:
    $result_data = array();
     /*
            BESUSH_ID, 
            DEVICE_HASH, 
            MANFACT_PREFIX, 
            LOC_ID, 
            VISIT_DATE, 
            START_TIME, 
            END_TIME, 
            COUNT, 
            MIN_SIGNAL, 
            MAX_SIGNAL are the keys in the assoc array $result_data
    */
    if(count($result) > 0){
        if(count($result) > 1){
            #fwrite($log_file, microtime() . ": Multiple results returned from BESUCH: need to analyze\n");
            $result_data = reduce($device, $result);
        }else{
            $result_data = $result[0];
            #fwrite($log_file, microtime() . ": Fetched result from BESUCH\n");
        }
        
        /*
            First comparison checks to see if the device instance is at the same location as the result: 
                If true, then device instance is not at the same location as result
                If false, then device instance is at the same location as result and needs to be evaluated for update
        */
        if($result_data['LOC_ID'] != $loc_id){
            #fwrite($log_file, microtime() . ": The fetched result is the same device but seen at a different location.\n");
            /*Since device_instance is not at the same location as result, need to check for a visit overlap*/
            if(visit_overlap($device->gTime(), $result_data['START_TIME'], $result_data['END_TIME'])){
                #fwrite($log_file, microtime() . ": Checked overlap and the incoming device timestamp is in between the start and end time of the returned result. Need to split visit\n");
                split_visit($result_data, $device);
            }
            /*After successful split then insert device instance*/
            $insert = bind_exec($db_connection, 'Insert', $handler_arr['insert'], $default_insert_params);
        }else{
            /*The device instance loc_id is the same as the result loc_id, need to evaluate and update fields if needed*/
            if(abs(find_closest($device, $result_data)) < VISIT_THRESHOLD || visit_overlap($device->gTime(), $result_data['START_TIME'], $result_data['END_TIME'])){
                /*
                    the device time and visit time is within the threshold then update 
                    OR
                    the device time is within the start and end time of the visit
                */
                $sql_update_code = update($device, $result_data);
                $update_sql = construct_update_sql($sql_update_code, $device, $loc_id);
                $update_result = mysqli_query($db_connection, $update_sql);
            }
            else{
                /*The device time and visit time is outside of the threshold and should just be inserted*/
                $insert = bind_exec($db_connection, 'Insert', $handler_arr['insert'], $default_insert_params);
            }
        }
    }else{ /*If the number of rows returned is 0, insert device instance into BESUCH*/
        $insert = bind_exec($db_connection, 'Insert', $handler_arr['insert'], $default_insert_params);
    }
}

function compare_signals($device_signal, $result_data){
    /*
        Compares the signal strength of the instance being passed and the min and max signals of the result passed, if the instance signal is greater or less then the max or min (respectively) then a value code is returned that identifies which field should be updated
    */
    $device_signal = intval($device_signal);
    $result_data['MIN_SIGNAL'] = intval($result_data['MIN_SIGNAL']);
    $result_data['MAX_SIGNAL'] = intval($result_data['MAX_SIGNAL']);
    $signal_code = -1;
    if($device_signal <= $result_data['MAX_SIGNAL'] && $device_signal >= $result_data['MIN_SIGNAL']){
        $signal_code = Update::NOTHING; /*device_signal is between the min and max signal*/
    }elseif($device_signal >= $result_data['MAX_SIGNAL'] && $device_signal >= $result_data['MIN_SIGNAL']){
        $signal_code = Update::MAX_SIGNAL;  /*device_signal is greater then max signal*/
    }else{
        $signal_code = Update::MIN_SIGNAL; /*device_signal is less then min signal*/
    }
    return $signal_code;
}

function update($device, $result_data){
    /*When updating the result, several fields need to be checked with the values of the device object
        Start_time and end_time are being compared to device visit_time
        Min_signal and max_signal are being comared to device visit_db
    */
    $signal_code = compare_signals($device->gVisit_DB(), $result_data);
    $device_time = new DateTime($device->gTime());
    $result_data['START_TIME'] = new DateTime($result_data['START_TIME']);
    $result_data['END_TIME'] = new DateTime($result_data['END_TIME']);
    $update_code = -1;

    if($device_time >= $result_data['START_TIME'] && $device_time <= $result_data['END_TIME']){
        $update_code = Update::COUNT;
    }elseif($device_time <= $result_data['START_TIME']){ 
        $update_code = Update::COUNT_START;
    }else{
        $update_code = Update::COUNT_END;
    }
    $update_code = $update_code + $signal_code;
    return $update_code;
}

function visit_overlap($device_time, $result_start_time, $result_end_time){
    /*
        Used to evaluate whether a device_time is in between a results start_time and end_time.
        device and result passed are assumed to have different loc_ids
        returns bool, overlap is true or overlap is false.
    */
    $overlap = false;
    $device_time = new DateTime($device_time);
    $result_start_time = new DateTime($result_start_time);
    $result_end_time = new DateTime($result_end_time);

    if($device_time >= $result_start_time && $device_time <= $result_end_time){
        $overlap = true;
    }
    return $overlap;
}

function construct_update_sql($update_code, $device, $loc_id){
    /*
        Based on the value code based a sql statement would be constructed to reflect the fields that need to be updated.
        It is assumed that count will always need to be incrememted by 1 with an update
    */

    $update_sql = "Update BESUCH set `COUNT` = `COUNT` + 1";
    $where_clause = " WHERE DEVICE_HASH = '".$device->gDevice_Hash()."' && LOC_ID = ".$loc_id." && VISIT_DATE = '".$device->gDate()."';";
    switch ($update_code){
        case Update::COUNT_START: /*Update start_time */
            $update_sql .= ", START_TIME = '".$device->gTime()."'";
            break;
        case Update::COUNT_END: /*Update end_time */
            $update_sql .= ", END_TIME = '".$device->gTime()."'";
            break;
        case Update::COUNT_MIN: /*Update min_signal */
            $update_sql .= ", MIN_SIGNAL = '".$device->gVisit_DB()."'";
            break;
        case Update::COUNT_MAX: /*Update max_signal */
            $update_sql .= ", MAX_SIGNAL = '".$device->gVisit_DB()."'";
            break;
        case Update::COUNT_START_MIN: /*Update start_time & min_signal */
            $update_sql .= ", START_TIME = '".$device->gTime()."', MIN_SIGNAL = '".$device->gVisit_DB()."'";
            break;
        case Update::COUNT_START_MAX: /*Update start_time & max_signal */
            $update_sql .= ", START_TIME = '".$device->gTime()."', MAX_SIGNAL = '".$device->gVisit_DB()."'";
            break;
        case Update::COUNT_END_MIN: /*Update end_time & min_signal*/
            $update_sql .= ", END_TIME = '".$device->gTime()."', MIN_SIGNAL = '".$device->gVisit_DB()."'";
            break;
        case Update::COUNT_END_MAX: /*Update end_time & max_signal*/
            $update_sql .= ", END_TIME = '".$device->gTime()."', MAX_SIGNAL = '".$device->gVisit_DB()."'";
            break;
    }
    $update_sql .= $where_clause;
    return $update_sql;
}

function split_visit($result_data, $device){
    /*
        Arguably one of the more complex functions.
        Function is called when script evaluates that result needs to be split to accomodate for the device instance that is being passed in.
        When conducting the split there needs to be several loose ends that have to be tied up, the best way to tie them up is by looking back through the visit table and finding the info
    */
    global $db_connection;
    //global $log_file;
    global $handler_arr;

    /*Finding the end_time for the left visit and the start_time for the right visit of the split*/
    
    //Left side
    $time_params = array("sis", $device->gDevice_Hash(), $result_data['LOC_ID'], $device->gVisit_Time());

    $end_time = bind_exec($db_connection, 'Select', $handler_arr['end_time'], $time_params);
    $end_time = $end_time[0]['VISIT_TIME'];

    #fwrite($log_file, microtime() . ": Found the end_time for the left side of the split: " . $end_time . "\n");

    //Right side
    $start_time = bind_exec($db_connection, 'Select', $handler_arr['start_time'], $time_params);
    $start_time = $start_time[0]['VISIT_TIME'];

    #fwrite($log_file, microtime() . ": Found the start_time for the right side of the split: ". $start_time . "\n");

    /*param arrays*/
    $left_side_params = array("siss", $device->gDevice_Hash(), $result_data['LOC_ID'], $device->gDate() . " " . $result_data['START_TIME'], $end_time);
    $right_side_params = array("siss", $device->gDevice_Hash(), $result_data['LOC_ID'], $start_time, $device->gDate() . " ". $result_data['END_TIME']);

    /*Finding signal for left split use $left_side_params*/
    $left_min_sig = bind_exec($db_connection, 'Select', $handler_arr['min_signal'], $left_side_params);
    $left_min_sig = $left_min_sig[0]['MIN'];

    #fwrite($log_file, microtime() . ": Found min signal for left split: ". $left_min_sig . "\n");

    $left_max_sig = bind_exec($db_connection, 'Select', $handler_arr['max_signal'], $left_side_params);
    $left_max_sig = $left_max_sig[0]['MAX'];

    #fwrite($log_file, microtime() . ": Found max signal for left split: ". $left_max_sig . "\n");

    /*Finding signal for right split use $right_side_params*/
    $right_min_sig = bind_exec($db_connection, 'Select', $handler_arr['min_signal'], $right_side_params);
    $right_min_sig = $right_min_sig[0]['MIN'];

    #fwrite($log_file, microtime() . ": Found min signal for right split: ". $right_min_sig . "\n");

    $right_max_sig = bind_exec($db_connection, 'Select', $handler_arr['max_signal'], $right_side_params);
    $right_max_sig = $right_max_sig[0]['MAX'];

    #fwrite($log_file, microtime() . ": Found max signal for right split: " . $right_max_sig . "\n");
    
    /*Finding count for left and right splits*/
    
    $left_count = bind_exec($db_connection, 'Select', $handler_arr['count'], $left_side_params);
    $left_count = $left_count[0]['COUNT'];

    #fwrite($log_file, microtime() . ": Found count for left split: " . $left_count . "\n");

    //Right side: use $right_side_params
    $right_count = bind_exec($db_connection, 'Select', $handler_arr['count'], $right_side_params);
    $right_count = $right_count[0]['COUNT'];

    #fwrite($log_file, microtime() . ": Found count for right split: " . $right_count . "\n");

    /*After finding the start_time end_time and count for the left split and right split, we need to put the pieces together and have this recorded in the database. Split is done by updating the current result row to reflect the left side and an insert statement that reflects the right side*/

    //Left side
    $update_result = bind_exec($db_connection, 'Update', $handler_arr['update'], array("siiisiss", $end_time, $left_count, $left_min_sig, $left_max_sig, $device->gDevice_Hash(), $result_data['LOC_ID'], $result_data['VISIT_DATE'], $result_data['END_TIME']));

    if($update_result <= 1){
        #fwrite($log_file, microtime() . ": Update successful, updated " . $update_result . " row");
    }else{
        throw new Exception("Miscaluated, update " . $update_result . " rows");
    }

    //Right side
    $insert_result = bind_exec($db_connection, "Insert", $handler_arr['insert'], array("ssisssiii", $device->gDevice_Hash(), $device->gManfact_Prefix(), $result_data['LOC_ID'], $device->gDate(), $start_time, $result_data['END_TIME'], $right_count, $right_min_sig, $right_max_sig));
    
    if($insert_result == 1){
        #fwrite($log_file, microtime() . ": Insert sucessful, affected " . $insert_result . " row");
    }else{
        throw new Exception("Insert did something funky, number of affected rows: " . $insert_result);
    }

    #fwrite($log_file, microtime() . " Splitting finished: new visits\n" . $device->gDevice_Hash() . "\nLeft start-" . $result_data['START_TIME'] . "\nLeft end time-" . $end_time . "\nMin & Max signal: " . $left_min_sig . " & " . $left_max_sig . "\nRight start time-" . $start_time . "\nRight end time-" . $result_data['END_TIME'] . "\nMin & Max signal: " . $right_min_sig . " " . $right_max_sig);
}

function prepare($db_connection, $query_arr){
    /*function takes a db_connection and an array of string queries, prepares them and then returns an array of handlers.*/
    $handler_arr;
    foreach($query_arr as $handler => $query){
        $handler_arr[$handler] = $db_connection->prepare($query);
    }
    return $handler_arr;
}

function bind_exec($db_connection, $query_type, $query_handler, $array_of_params){
    /*Abstract function that streamlines the process of binding, executing and retrieving statements that are being sent to the database.*/
    if(call_user_func_array(array($query_handler, "bind_param"), refValues($array_of_params))){
        $result = false;
        switch ($query_type):
            case "Select":
                $result = $query_handler->execute();
                if($result){
                    $result_set = $query_handler->get_result();
                    $result = $result_set->fetch_all(MYSQLI_ASSOC);
                }
                break;
            case "Update":
                $result = $query_handler->execute();
                if($result){
                    $result = $query_handler->affected_rows;
                }
                break;
            case "Insert":
                $result = $query_handler->execute();
                if($result){
                    $result = $query_handler->affected_rows;
                }
                break;
            default:
                echo "Using restricted sql keyword: Execute failed\n";
        endswitch;
        $query_handler->free_result();
        return $result;
    }
    else{ throw new Exception("Binding failed: (" . $query_handler->errno . ") " . $query_handler->error); }
}

function refValues($arr){
    //returns array by reference, allows usage of call_user_func_array()
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    return $arr;
}

function reduce($device, $result_object){
    /*This process is used to help find which result (visit) is the closest and most relevent in relation to the device object passed in*/

    global $log_file;
    //time difference & array reference variables
    $left_closest = SECS_N_DAY * -1;
    $right_closest = SECS_N_DAY;
    $left_ref = -1;
    $right_ref = -1;

    foreach($result_object as $key=>$result){

        if(visit_overlap($device->gTime(), $result['START_TIME'], $result['END_TIME'])){
            //If device has an overlap with any of the results, that takes precedence over everything and needs to be returned
            $result['eval'] = 0;
            return $result;
        }
        else{
            /*
                The time difference of the device and result is returned. value and array reference is saved based on value is negative or positive
            */
            $result['eval'] = find_closest($device, $result);
            if($result['eval'] > 0){ /*'eval' is positive */
                if($result['eval'] < $right_closest){ /*Means that 'eval' is closer and will be saved as the new closest*/
                    $right_closest = $result['eval'];
                    $right_ref = $key;
                } 
            }else{ /*'eval' is negative*/
                if($result['eval'] > $left_closest){ /*Means that 'eval' is closer and will be saved as the new closest*/
                    $left_closest = $result['eval'];
                    $left_ref = $key;
                }
            }
        }
    }
    /* Right closest and left closest are found. Need to compare the difference between the left and right closest to figure out if either left or right is what should be updated in relation to the device instance */
    //Needs to be a threshold catch and a location_id check to script
    if(abs($right_closest) > abs($left_closest)){ /*Left time is closer to 0*/
        return $result_object[$left_ref];
    }else{ /*This means that the start time is closer to the device time*/
        return $result_object[$right_ref];
    }
}

function find_closest($device, $result){
    /*Function finds which of result start_time and end_time variables is closer to device time. Closest is then returned*/

    $start_device_diff = timediff($device->gVisit_Time(), $result['VISIT_DATE'] . " " . $result['START_TIME']);

    $end_device_diff = timediff($device->gVisit_Time(), $result['VISIT_DATE'] . " " . $result['END_TIME']);
    //Take the abs of each value to find the closest
    if(abs($start_device_diff) > abs($end_device_diff)){ /*This means that end time is closer to the device time*/
        return $end_device_diff;
    }else{ /*This means that the start time is closer to the device time*/
        return $start_device_diff;
    }
}

function timediff($device_time, $instance_time){
    //assumed that the date will be the same for both times
    $device_time = new DateTime($device_time);
    $instance_time = new DateTime($instance_time);
    $diff = $device_time->diff($instance_time);
    //DateInterval obj returned
    $time_diff = ($diff->h * CONVERSION * CONVERSION) + ($diff->i * CONVERSION) + $diff->s;
    if($diff->invert == TRUE)
    {
        $time_diff = $time_diff * -1;
    }
    return $time_diff;
}

//Start of test:
//For the test, visits are pulled straight from the visit table, after their pi mac has been resolved into a location id number, code will be slightly modified to accomodate for that, live verison will be getting messages that still contain their pi mac address
//$db_connection = connect_w_db();
$db_connection = connect_w_db();
$query_arr = array(
    "visits" => "SELECT * FROM BESUCH WHERE DEVICE_HASH = ? && VISIT_DATE = ?",
    "end_time" => "SELECT VISIT_TIME FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME < ? ORDER BY VISIT_TIME DESC LIMIT 1",
    "start_time" => "SELECT VISIT_TIME FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME > ? ORDER BY VISIT_TIME ASC LIMIT 1",
    "min_signal" => "SELECT MIN(VISIT_DB) as MIN FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME >= ? && VISIT_TIME <= ?",
    "max_signal" => "SELECT MAX(VISIT_DB) as MAX FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME >= ? && VISIT_TIME <= ?",
    "count" => "SELECT count(*) as COUNT FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME >= ? && VISIT_TIME <= ?",
    "update" => "UPDATE BESUCH SET END_TIME = cast(? as time), COUNT = ?, MIN_SIGNAL = ?, MAX_SIGNAL = ? WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_DATE = ? && END_TIME <= ? ORDER BY END_TIME DESC LIMIT 1",
    "insert" => "INSERT INTO BESUCH (DEVICE_HASH, MANFACT_PREFIX, LOC_ID, VISIT_DATE, START_TIME, END_TIME, `COUNT`, MIN_SIGNAL, MAX_SIGNAL) VALUES (?,?,?,?, cast(? as time),?,?,?,?)",
);
$handler_arr = prepare($db_connection, $query_arr);

//$log_file = fopen("log.txt", "w");
$count = 1;

if($result = $db_connection->query("SELECT * FROM VISIT WHERE VISIT_TIME BETWEEN '2016-09-01' AND '2016-09-30'")){
    #fwrite($log_file, microtime() . ": Selecting all from visit table\n");
    while($row = $result->fetch_row()){
        #fwrite($log_file, microtime() . ": Fetching a row\n");
        echo $count . "/" . $result->num_rows . "\n";
        $device = create_Device($row[3], $row[1], $row[2], $row[4], $row[5], "N/A", "N/A");
        process($device);
        $count = $count + 1;
    }
}
$result->close();
?>