<?php
header('Content-Type: text/html; charset=utf-8');

#Build_Database.php
#Creates database, tables, with appropriate keys/index
require 'connect.php';

$db_connection = connect_sans_db();
//Change working database in database.ini
$config = parse_ini_file('../Credentials/credentials.ini');
$db_name = $config['database'];

//function to check if an index has been created or not, returns number of columns used for index, 0 if not created
function checkIndex($server, $db, $table, $index)
{
	$sql = "SELECT COUNT(1) IndexIsThere FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema='" . $db . "' AND table_name='" . $table . "' AND index_name='" . $index . "';";
	return mysqli_query($server, $sql)->fetch_object()->IndexIsThere;
}

//Checks if DB is created, sets as working DB
$sql = "CREATE DATABASE IF NOT EXISTS " . $db_name . " DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;";
mysqli_query($db_connection, $sql);
mysqli_select_db($db_connection, $db_name);

//Creates LOCATION table, table contains location names
$sql = "CREATE TABLE IF NOT EXISTS `LOCATION`(`LOC_ID` bigint(20) NOT NULL AUTO_INCREMENT,`LOC_NAME` varchar(255) NOT NULL, PRIMARY KEY(LOC_ID));";
mysqli_query($db_connection, $sql);

//Creates VISIT table, table contains raw info from tshark
$sql = "CREATE TABLE IF NOT EXISTS `VISIT`(`VISIT_ID` bigint(20) NOT NULL AUTO_INCREMENT, `DEVICE_HASH` varchar(128) NOT NULL, MANFACT_PREFIX varchar(17) NOT NULL, `LOC_ID` bigint(20) NOT NULL, `VISIT_TIME` datetime NOT NULL, `VISIT_DB` smallint(6) NOT NULL, PRIMARY KEY(VISIT_ID), FOREIGN KEY (LOC_ID) REFERENCES LOCATION(LOC_ID));";
mysqli_query($db_connection, $sql);

//Creates PI_LOC_MAP table, table maps Pi to Loc
$sql = "CREATE TABLE IF NOT EXISTS `PI_LOC_MAP`(`PI_MAC` varchar(17) NOT NULL, `LOC_ID` bigint(20), PRIMARY KEY (PI_MAC), FOREIGN KEY (LOC_ID) REFERENCES LOCATION(LOC_ID));";
mysqli_query($db_connection, $sql);

//Creates BLACKLIST table, devices that are present constantly within pi range will be ignored and placed in this table
$sql = "CREATE TABLE IF NOT EXISTS `BLACKLIST`(`BLCK_ID` bigint(20) NOT NULL AUTO_INCREMENT, `BLCK_DEVICE` varchar(255) NOT NULL, PRIMARY KEY (BLCK_ID));";
mysqli_query($db_connection, $sql);

//CREATES BESUCH table, massaged data that can better represent a device over time as oppose to an instance
$sql = "CREATE TABLE IF NOT EXISTS `BESUCH`(`BESUCH_ID` bigint(20) NOT NULL AUTO_INCREMENT, `DEVICE_HASH` varchar(255) NOT NULL, `MANFACT_PREFIX` varchar(8) NOT NULL, `LOC_ID` bigint(20) NOT NULL, `VISIT_DATE` date, `START_TIME` time, `END_TIME` time, `COUNT` int(11), `MIN_SIGNAL` smallint(6), `MAX_SIGNAL` smallint(6), PRIMARY KEY (BESUCH_ID), FOREIGN KEY (LOC_ID) REFERENCES LOCATION(LOC_ID));";
mysqli_query($db_connection, $sql);

//Creates SIGN_IN table, table used to record status of PI, easy way to see if pi is active or not
$sql = "CREATE TABLE IF NOT EXISTS `SIGN_IN`(`SIGN_IN_ID` bigint(20) NOT NULL AUTO_INCREMENT, `PI_MAC` varchar(17)  NOT NULL, `LOC_ID` bigint(20) NOT NULL, `SIGN_IN_TIME` datetime, PRIMARY KEY(SIGN_IN_ID), FOREIGN KEY (PI_MAC) REFERENCES PI_LOC_MAP(PI_MAC), FOREIGN KEY(LOC_ID) REFERENCES LOCATION(LOC_ID));";
mysqli_query($db_connection, $sql);

//Creates INEDEXES for tables
$answer = checkIndex($db_connection, $db_name, "BESUCH", "DEVICE_VISIT");
if($answer == 0)
{
	$sql = "CREATE INDEX DEVICE_VISIT ON BESUCH(`DEVICE_HASH`, `LOC_ID`, `VISIT_DATE`);";
	mysqli_query($db_connection, $sql);
}
$answer = checkIndex($db_connection, $db_name, "BESUCH", "DEVICE");
if($answer == 0)
{
	$sql = "CREATE INDEX DEVICE ON BESUCH(`DEVICE_HASH`, `VISIT_DATE`);";
	mysqli_query($db_connection, $sql);
}
$answer = checkIndex($db_connection, $db_name, "VISIT", "DEVICE_INSTANCE");
if($answer == 0)
{
	$sql = "CREATE UNIQUE INDEX DEVICE_INSTANCE ON VISIT(`DEVICE_HASH`, `LOC_ID`, `VISIT_TIME`);";
	mysqli_query($db_connection, $sql);
}
$answer = checkIndex($db_connection, $db_name, "PI_MAC", "LOC_ID");
if($answer == 0)
{
	$sql = "CREATE UNIQUE INDEX METER_LOCATION ON SIGN_IN(`PI_MAC`, `LOC_ID`);";
	mysqli_query($db_connection, $sql);
}

//Add more php/sql as need be, this file can be run many times, any added code should check for existence before adding to database
mysqli_close($db_connection);
?>


