<?php
header('Content-Type: application/json; charset=utf-8');
#db_write.php
#file called after ruby script polls a message from aws queue, ruby file needs to somehow pass the information from the sqs message here to get process and them dump into the DB
require 'db_functions.php';
require 'connect.php';

//opens file that sqs_poll writes to containing device info
$file_name = fgets(STDIN);
$file = fopen("../Receiving/" . $file_name, "r") or die ("Unable to open file");
$argument = fgets($file);
fclose($file);

$json_devices = json_decode($argument);
$devices = array();
$db_handler = connect_w_db();

//takes each json object and creates a device object which is pushed into an array
foreach($json_devices as $device)
{
	//This may be redundant to cast as an object, wasting the over all time it takes to insert
	//To insert 500 device instances it takes approx 45 seconds, might look into making that faster
	$device_obj = create_Device($device->Pi_Mac, $device->Device_Hash, $device->Manfact_Prefix, $device->Visit_Time, $device->Visit_DB, $device->Message_Id, $device->Receipt_Handle);
	$devices[] = $device_obj;
}

//Expect to get back an array of all the message_ids and reciept handles of the messages that have successfully written to the database
$array_encoded = json_encode(insert_many_in_Visit($db_handler, $devices));
echo $array_encoded;
mysqli_close($db_handler);
?>