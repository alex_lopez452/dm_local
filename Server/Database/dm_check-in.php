<?php
header('Content-Type: application/json; charset=utf-8');
#dm_check-in.php
#file called after ruby script polls a message from aws queue, ruby file needs to somehow pass the information from the sqs message here to get process and them dump into the DB
#file gets called with one line piped to the STDIN, processes that line and then exits out
require 'db_functions.php';
require 'connect.php';

$db_handler = connect_w_db();
$meter_status = json_decode(fgets(STDIN));

$result = sign_in($db_handler, $meter_status->Pi_Mac, $meter_status->Timestamp);

if($result){
	$object = json_encode(create_receipt($meter_status->Message_id, $meter_status->Receipt_Handle));
	echo $object;
}