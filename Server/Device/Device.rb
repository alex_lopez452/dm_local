require 'json'

#Class file for the devices stored in the database


class Device
	attr_accessor :Pi_Mac, :Device_Hash, :Manfact_Prefix, :Visit_Time, :Visit_DB, :Message_ID, :Receipt_Handle

	def initialize(pi_mac, device_hash, manfact_prefix, visit_time, visit_db, message_id, receipt_handle)
		@Pi_Mac = pi_mac
		@Device_Hash = device_hash
		@Manfact_Prefix = manfact_prefix
		@Visit_Time = visit_time
		@Visit_DB = visit_db
		@Message_ID = message_id
		@Receipt_Handle = receipt_handle
	end

	def show
		puts "Instance method show invoked for 
		#{self}"
	end

	def to_s
		"Pi_Mac: #{Pi_Mac}, Device_Hash: #{Device_Hash}, Manfact_Prefix: #{Manfact_Prefix}, Visit Time: #{Visit_Time}, Signal: #{Visit_DB}"
	end

	def to_json
		{'Device_ID' => device_id, 'MAC_Prefix' => encrypt_addr(mac_prefix), 'Location_Name' => get_location_name(location_id), 'visit_time' => visit_time, 'visit_db' => visit_db}.to_json
	end
end