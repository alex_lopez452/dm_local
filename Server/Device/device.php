<?php
header('Content-Type: text/html; charset=utf-8');

class device
{
	var $pi_mac;
	var $device_hash;
	var $manfact_prefix;
	var $visit_time; //combination of date + time
	var $visit_db;
	var $message_id;
	var $receipt_handle;
	var $time;
	var $date;

	function __construct($Pi_Mac, $Device_Hash, $Manfact_Prefix, $Visit_Time, $Visit_DB, $Message_Id, $Receipt_handle)
	{
		$this -> pi_mac = $Pi_Mac;
		$this -> device_hash = $Device_Hash;
		$this -> manfact_prefix = $Manfact_Prefix;
		$this -> visit_time = $Visit_Time;
		$this -> visit_db = $Visit_DB;
		$this -> message_id = $Message_Id;
		$this -> receipt_handle = $Receipt_handle;
	}

	function gPi_Mac()
	{
		return $this -> pi_mac;
	}

	function gDevice_Hash()
	{
		return $this -> device_hash;
	}

	function gManfact_Prefix()
	{
		return $this -> manfact_prefix;
	}

	function gVisit_Time()
	{
		return $this -> visit_time;
	}

	function gVisit_DB()
	{
		return $this -> visit_db;
	}

	function gMessage_Id()
	{
		return $this -> message_id;
	}

	function gReceiptHandle()
	{
		return $this -> receipt_handle;
	}

	function gTime()
	{
		return $this -> time;
	}

	function gDate()
	{
		return $this -> date;
	}

	function sTime($t)
	{
		$this -> time = $t;
	}

	function sDate($d)
	{
		$this -> date = $d;
	}

	function to_s()
	{
		return "Pi_Mac: " + $pi_mac + " Device_Hash: " + $device_hash + " Manfact_Prefix: " + $manfact_prefix + " Visit_Time: " + $visit_time + " Visit_DB: " + $visit_db;
	}
}

?>