<?php
header('Content-Type: text/html; charset=utf-8');
#meter_notfication.php
#File sends out an email to the recipients listed if a meter has not sent any information in a given period of time
//NEEDS A LAUNCHER FILE FOR CRON

define('REQUIRED_FILE', '../aws/aws-autoloader.php');
define('CONNECT_FILE', '../Database/connect.php');
define('DB_HELPER_FILE', '../Database/db_functions.php');
define('SENDER', 'destinationmeter@gmail.com');
define('RECIPIENT', 'al4526675@gmail.com');
define('REGION', 'us-east-1');
define('SUBJECT', 'Meter Notifcation');
define('HOURS', 24);
define('THRESHOLD', 3);

require REQUIRED_FILE;
require CONNECT_FILE;
require DB_HELPER_FILE;
use Aws\Ses\SesClient;

$config = parse_ini_file('../Credentials/credentials.ini');

$client = SesClient::factory(array(
	'version' => 'latest',
	'region' => REGION, 
	'credentials' => [
		'key' => $config['aws_access_key_id'],
		'secret' => $config['aws_secret_access_key']
	]
));

//To send an email the argument is an assoc array with specific fields filled in with information, things like sender, reciever, and the body is all contained in that assoc array
$request = array();
$request['Source'] = SENDER;
$request['Destination']['ToAddresses'] = array(RECIPIENT);
$request['Message']['Subject']['Data'] = SUBJECT;

$db_connection = connect_w_db();
$now = new DateTime();
$num_dwnMeters = 0;
//SQL Magic, pulls last uploaded record & sign in time, for each location id
$sql = "SELECT SIGN_IN.LOC_ID, SIGN_IN.SIGN_IN_TIME, last_uploaded_table.last_uploaded FROM SIGN_IN INNER JOIN (SELECT VISIT.LOC_ID, max(VISIT_TIME) last_uploaded FROM VISIT group by VISIT.LOC_ID) as last_uploaded_table on SIGN_IN.LOC_ID = last_uploaded_table.LOC_ID group by SIGN_IN.LOC_ID;";
$result = mysqli_query($db_connection, $sql);

$body = "<h1>Meter(s) at the following locations may need to be serviced.</h1>\n\n<table style=\" border-spacing:12px\"><tr><th>Meter location name:</th><th>Last Sign In:</th><th>Last Uploaded</th></tr>";
while($row = mysqli_fetch_assoc($result)){
	//the result $row will contain $row['LOC_ID'], $row['SIGN_IN_TIME'], $row['last_uploaded']
	$sign_in = new DateTime($row['SIGN_IN_TIME']);
	$last_up = new DateTime($row['last_uploaded']);
	$meter_flag = false;

	$SIn_now_diff = $now->diff($sign_in);
	$Sn_hour_diff = $SIn_now_diff->h + ($SIn_now_diff->days * HOURS);

	if($Sn_hour_diff <= THRESHOLD){
		//Will measure time difference between last uploaded and sign in
		$SIn_LUp_diff = $last_up->diff($sign_in);
		$SL_hour_diff = $SIn_LUp_diff->h + ($SIn_LUp_diff->days * HOURS);
		
		if($SL_hour_diff >= THRESHOLD){
			//If the difference between sign in and last uploaded is greater then 3 hours then the meter has stopped collecting information. Email needs to be sent out
			$num_dwnMeters = ++$num_dwnMeters;
			$meter_flag = true;
		}
	}else{
		//This means that the meter was last seen more then three hours ago, email needs to be sent out with the proper information. $body will contain the basic text of the email
		$num_dwnMeters = ++$num_dwnMeters;
		$meter_flag = true;
	}

	if($meter_flag){
		$body = $body . "<tr><td>" . find_LOC_name($db_connection, $row['LOC_ID']) . "</td><td>" . $row['SIGN_IN_TIME'] . "</td><td>" . $row['last_uploaded'] . "</td></tr>";
	}
}

$body = $body . "</table>";

if($num_dwnMeters > 0){
	$request['Message']['Body']['Html']['Data'] = $body;
	try {
		$result = $client->sendEmail($request);
		$messageId = $result->get('MessageId');
	//echo("Email sent! Message ID: " . $messageId . "\n");
	}catch (Exception $e) {
		echo("The email was not sent. Error message: ");
		echo ($e->getMessage()."\n");
	}
}
?>