
class Message_handler
	attr_accessor :message_id, :receipt_handle

	def initialize(message_id, receipt_handle)
		@message_id = message_id
		@receipt_handle = receipt_handle
	end

	def message_id
		@message_id
	end

	def receipt_handle
		@receipt_handle
	end
end

