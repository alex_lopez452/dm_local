require 'aws-sdk'
require 'inifile'
require 'json'
require 'open3'
require './message_handler.rb'

#sqs_heartbeat_poll.rb
#Works similiar to sqs_poll.rb, sends the information to dm_check-in.php sticks it in the database and then returns a receipt handle to delete the message from SQS
Aws.config[:ssl_ca_bundle] = '../Credentials/ca-bundle.crt'

config = IniFile.load('../Credentials/credentials.ini')

sqs = Aws::SQS::Client.new(
	region: 'us-east-1',
	access_key_id: "#{config['aws_IAM']['aws_access_key_id']}",
	secret_access_key:"#{config['aws_IAM']['aws_secret_access_key']}"
	)
url = "#{config['SQS_URL']['sqs_heartbeat_url']}"

poller = Aws::SQS::QueuePoller.new(url, {client: sqs})

poller.poll(visibility_timeout: 120, skip_delete: true) do |msg|
	
	meter_status = { 
		Pi_Mac: msg.body.strip, 
		Timestamp: msg.message_attributes['Time'][:string_value].strip, 
		Message_id: msg.message_id, 
		Receipt_Handle: msg.receipt_handle
	}.to_json
	#puts "#{meter_status}"
	check_in_status = Array.new
	Open3.popen3("php", "../Database/dm_check-in.php") do |stdin, stdout, stderr, wait_thr|
		stdin.puts "#{meter_status}"
		stdin.close
		check_in_status = stdout.read #returns message id and receipt handle for messages successfully inserted or updated
		error_messages = stderr.read
		#puts "#{error_messages}"
		#don't really do anything with the errors, might adjust this later
	end
	if !check_in_status.empty?
		check_in_status = JSON.parse(check_in_status)
		#puts "#{check_in_status}"
		poller.delete_message(Message_handler.new(check_in_status["message_id"], check_in_status["receipt_handle"]))
	end
end