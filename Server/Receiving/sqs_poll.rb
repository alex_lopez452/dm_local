require 'aws-sdk'
require 'inifile'
require 'json'
require 'open3'
require '../Device/Device.rb'
require './message_handler.rb'

#sqs_poll.rb
#polls from AWS queue and strips the message based on an agreed message format, 
#then stored to a file and read from db_write.php when it is launched,
#db_write.php takes the information and writes it to the database,
#successfully inserted messages are then returned to be deleted.
Aws.config[:ssl_ca_bundle] = '../Credentials/ca-bundle.crt'

config = IniFile.load('../Credentials/credentials.ini')

sqs = Aws::SQS::Client.new(
	region: 'us-east-1',
	access_key_id: "#{config['aws_IAM']['aws_access_key_id']}",
	secret_access_key:"#{config['aws_IAM']['aws_secret_access_key']}"
	)
url = "#{config['SQS_URL']['sqs_url']}"


def Start_polling(file_name, sqs, url)
	poller = Aws::SQS::QueuePoller.new(url, {client: sqs})
	devices = Array.new

	puts "Starting polling for #{file_name}"
#Make sure to delete idle_timeout once you are ready to deploy
	begin
		poller.poll(visibility_timeout: 600, skip_delete:true) do |msg|
		#Look at message_format.txt to understand the format of the sqs message
		#creates a new device object and pushes it into the array
			devices.push(Device.new(
				msg.body, 
				msg.message_attributes['Hash'][:string_value],
				msg.message_attributes['Prefix'][:string_value],
				msg.message_attributes['Time'][:string_value],
				msg.message_attributes['Db'][:string_value],
				msg.message_id,
				msg.receipt_handle
				)
			)
		
		#adjust the total number of devices you want to pass in once you are ready to deploy
			if devices.size >= 500
			#once the array reaches the condition size it will pass that array, now converted to json, to db_write.php to write the devices into the table
				json_devices = devices.map { |device| 
					{
						Pi_Mac: device.Pi_Mac, 
						Device_Hash: device.Device_Hash, 
						Manfact_Prefix: device.Manfact_Prefix, 
						Visit_Time: device.Visit_Time, 
						Visit_DB: device.Visit_DB, 
						Message_Id: device.Message_ID, 
						Receipt_Handle: device.Receipt_Handle
					} 
				}.to_json
				devices.clear
				File.open("#{file_name}.txt", "w") { |f| f.write("#{json_devices}") }

				raw_messages = Array.new
				messages = Array.new
			#To confirm a proper deletion of the message I will return the message ID needed to delete the message
			
				Open3.popen3("php", "../Database/db_write.php") do |stdin, stdout, stderr, wait_thr|
					stdin.write "#{file_name}.txt"
					stdin.close
					json_messages = stdout.read
					error_messages = stderr.read
					puts "#{error_messages}"
					raw_messages = JSON.parse(json_messages)
				#rescue JSON::ParserError
				#what if json_messages is empty? should think about error checking in the future
				#don't really do anything with the errors, might adjust this later
				end

				File.delete("#{file_name}.txt")
				raw_messages.each do |msg|
					messages.push(Message_handler.new(msg["message_id"], msg["receipt_handle"]))
				#Batch deletion only allows a max of 10 messages at a time
					if messages.size == 10
						poller.delete_messages(messages)
						messages.clear
					end
				end
			#safety check to delete anything that might be left over
				if messages.size > 0
					poller.delete_messages(messages)
				end
			end
		end
	rescue Seahorse::Client::NetworkingError
	#Thought, since this is the server code, and amazon's server were to ever lose connection should I just put a timeout for the script and wait a little before trying again?
		puts "Server network not working, restarting"
	end
end

#request = sqs.GetQueueAttributesRequest({queue_url: url, attribute_names: ["All"]})
t1 = Thread.new{Start_polling("file1", sqs, url)}
t2 = Thread.new{Start_polling("file2", sqs, url)}
t3 = Thread.new{Start_polling("file3", sqs, url)}
t4 = Thread.new{Start_polling("file4", sqs, url)}
t5 = Thread.new{Start_polling("file5", sqs, url)}
t1.join
t2.join
t3.join
t4.join
t5.join