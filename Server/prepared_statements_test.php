<?php
$query_arr = array(
    "visits" => "SELECT * FROM BESUCH WHERE DEVICE_HASH = ? && VISIT_DATE = ?",
    "end_time" => "SELECT VISIT_TIME FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME < ? ORDER BY VISIT_TIME DESC LIMIT 1",
    "start_time" => "SELECT VISIT_TIME FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME > ? ORDER BY VISIT_TIME ASC LIMIT 1",
    "min_signal" => "SELECT MIN(VISIT_DB) as MIN FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME >= ? && VISIT_TIME <= ?",
    "max_signal" => "SELECT MAX(VISIT_DB) as MAX FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME >= ? && VISIT_TIME <= ?",
    "count" => "SELECT count(*) as COUNT FROM VISIT WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_TIME >= ? && VISIT_TIME <= ?",
    "update" => "UPDATE BESUCH SET END_TIME = cast(? as time), COUNT = ?, MIN_SIGNAL = ?, MAX_SIGNAL = ? WHERE DEVICE_HASH = ? && LOC_ID = ? && VISIT_DATE = ? && END_TIME <= ? ORDER BY END_TIME DESC LIMIT 1",
    "insert" => "INSERT INTO BESUCH (DEVICE_HASH, MANFACT_PREFIX, LOC_ID, VISIT_DATE, START_TIME, END_TIME, `COUNT`, MIN_SIGNAL, MAX_SIGNAL) VALUES (?,?,?,?, cast(? as time),?,?,?,?)",
);

function prepare($db_connection, $query_arr){
    /*function takes a db_connection and an array of queries, prepares them and then returns an array of handlers.*/
    $handler_arr;

    foreach($query_arr as $handler => $query){
        $handler_arr[$handler] = $db_connection->prepare($query);
    }
    return $handler_arr;
}

function db_connect_local()
{
    $db_aws_hostname = "localhost:3306";
    $db_aws_database = "v2.test";
    $db_aws_username = "root";
    $db_aws_password = "root";
    
    static $connection;
    if(!isset($connection))
    {
        $connection = mysqli_connect($db_aws_hostname, $db_aws_username, $db_aws_password, $db_aws_database);
    }
    if($connection === false)
    {
        return mysqli_connect_error(); 
    }
    return $connection;
}

function refValues($arr){
    //returns array by reference, allows usage of call_user_func_array()
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    return $arr;
}

function bind($db_connection, $query_handler, $array_of_params){
    /*Abstract function that streamlines the process of preparing, binding, executing and retrieving statements that are being sent to the database.*/
    if(call_user_func_array(array($query_handler, "bind_param"), refValues($array_of_params))){
        var_dump($query_handler);
    }
    else{ throw new Exception("Binding failed: (" . $query_handler->errno . ") " . $query_handler->error); }
}
$db_connection = db_connect_local();
$handler_arr = prepare($db_connection, $query_arr);

bind($db_connection, $handler_arr['visits'], array("ss", "aa:aa:aa:aa:aa:aa", "2017-09-02"))

?>