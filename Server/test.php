<?php
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('America/New_York');

function db_connect_local()
{
    $db_aws_hostname = "localhost:3306";
    $db_aws_database = "v2.test";
    $db_aws_username = "root";
    $db_aws_password = "root";
    
    static $connection;
    if(!isset($connection))
    {
        $connection = mysqli_connect($db_aws_hostname, $db_aws_username, $db_aws_password, $db_aws_database);
    }
    if($connection === false)
    {
        return mysqli_connect_error(); 
    }
    return $connection;
}

function prepare_N_bind($db_connection, $query_type, $query_string, $array_of_params){
    $query_handler = $db_connection->stmt_init();
    if($query_handler = $db_connection->prepare($query_string)){
        if(call_user_func_array(array($query_handler, "bind_param"), refValues($array_of_params))){
            $result = false;
            switch ($query_type):
            	case "Select":
            		$result = $query_handler->execute();
            		if($result){
            			$result_set = $query_handler->get_result();
            			$result = $result_set->fetch_all(MYSQLI_ASSOC);
            		}
            		break;
            	case "Update":
            		$result = $query_handler->execute();
            		if($result){
            			$result = $query_handler->affected_rows;
            		}
            		break;
            	case "Insert":
            		$result = $query_handler->execute();
            		if($result){
            			$result = $query_handler->affected_rows;
            		}
            		break;
            	default:
            		echo "Using restricted sql keyword: Execute failed\n";
            endswitch;
            $query_handler->close();
            return $result;
        }
        else{ echo "Binding failed: (" . $query_handler->errno . ") " . $query_handler->error; }
    }else{ echo "Prepared query failed: (" . $query_handler->errno . ") " . $query_handler->error; }
}

$db_connection = db_connect_local();
$query_type = "meow";
$query_string = "SELECT * FROM VISIT WHERE LOC_ID = ? LIMIT 50";
$array_of_params = array('i', 8);

$value2 = prepare_N_bind($db_connection, 'Select', $query_string, $array_of_params);
var_dump($value2);

function refValues($arr){
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    return $arr;
}
?>